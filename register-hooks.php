#!/usr/bin/php
<?php

$roboPath = './vendor/bin/robo';
$hooksPath = __DIR__ . '/.git/hooks';
if (!file_exists($hooksPath)) {
    mkdir($hooksPath, 0755, true);
}
touch($hooksPath . '/pre-commit');
fwrite(fopen('./.git/hooks/pre-commit', 'w'),
        "#!/bin/sh\n\n$roboPath pre:commit\n");
chmod($hooksPath . '/pre-commit', 0755);

unlink(__FILE__);

exit(0);

