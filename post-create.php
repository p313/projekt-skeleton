#!/usr/bin/php
<?php
// We get the project name from the name of the path that Composer created for us.
$project = basename(realpath("."));
echo "Project name $project taken from directory name\n";

$projectSlug = strtolower(str_replace(' ', '-',
                trim(preg_replace('/([A-Z])/', ' \\1', $project))));
echo "Project slug is $projectSlug\n";

$replaces = [
    '"name": "p313/projekt-skeleton"' => '"name": "p313/' . $projectSlug . '"',
    '"description": "A skeleton project to help bootstrap new libraries within P313"' => '"description": ""',
    '
        "post-create-project-cmd": [
            "composer update",
            "./vendor/bin/codecept bootstrap && ./vendor/bin/codecept build",
            "./post-create.php",
            "git init",
            "git add .",
            "git commit -m \"Initial commit\"",
            "git-flow init -d"
        ]' => ''
];
$composer = file_get_contents('./composer.json');
foreach ($replaces as $before => $after) {
    $composer = str_replace($before, $after, $composer);
}

$roboPath = './vendor/bin/robo';
mkdir('build');

fwrite(fopen('./composer.json', 'w'), $composer);
fwrite(fopen('./README.md', 'w'), "# $project\n\nRead me.");
fwrite(fopen('./test/unit.suite.yml', 'w'),
    "# Codeception Test Suite Configuration
#
# Suite for unit (internal) tests.

class_name: UnitTester
modules:
    enabled:
        - Asserts
        - \Helper\Unit
coverage:
    enabled: true
    whitelist:
        include:
            - src/*
");

unlink(__FILE__);

exit(0);

