<?php

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{

    use Robo\Task\Base\loadShortcuts;

    function getTaskCodecept($coverage = true, $failFast = false)
    {
        $task = $this->taskCodecept();

        if ($failFast) {
            $task->option('fail-fast');
        }

        if ($coverage) {
            $task->coverageHtml();
        }

        return $task;
    }

    function tests($opts = ['silent|s' => false, 'coverage|c' => true])
    {
        $result = $this->getTaskCodecept($opts['coverage'])
                ->run();

        // print message when tests passed
        if (!$silent && $result->wasSuccessful()) {
            $this->say("All tests successful");
        }

        return $result;
    }

    function check()
    {
        $tasks = [
            'tests' => $this->getTaskCodecept(),
            'cs' => $this->taskExec('phpcs --report=xml --standard=PSR1,PSR2 src | tee build/phpcs.xml'),
            'loc' => $this->taskExec('phploc --log-xml=build/phploc.xml src'),
            'md' => $this->taskExec('phpmd src xml cleancode,codesize,controversial,design,naming,unusedcode | tee build/pmd.xml'),
            'docs' => $this->taskExec('phpdox'),
        ];

        return $this->collectionBuilder()
                        ->addTaskList($tasks);
    }

    function preCommit()
    {
        $tasks = [
            'tests' => $this->getTaskCodecept(false, true),
            'cs' => $this->taskExec('phpcs --report=xml --standard=PSR1,PSR2 src | tee build/phpcs.xml'),
            'loc' => $this->taskExec('phploc --log-xml=build/phploc.xml src'),
            'md' => $this->taskExec('phpmd src xml cleancode,codesize,controversial,design,naming,unusedcode | tee build/pmd.xml'),
            'docs' => $this->taskExec('phpdox'),
            'commit' => $this->taskGitStack()
                    ->stopOnFail()
                    ->add('docs/')
                    ->add('tests')
        ];

        return $this->collectionBuilder()
                        ->addTaskList($tasks);
    }
}
